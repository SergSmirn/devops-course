#!/bin/sh
maxcounter=45

check_mysql() {
    counter=1
    until nc -z -v -w30 db 3306; do
    echo "tick"
    sleep 1
    counter=`expr $counter + 1`
    if [ $counter -gt $maxcounter ]; then
        >&2 echo "We have been waiting for MySQL too long already; failing."
        exit 1
    fi;
    done
}

echo "run"
check_mysql()
echo "first check"
sleep 5
check_mysql()
echo "second check"
python manage.py runserver 0.0.0.0:8000
